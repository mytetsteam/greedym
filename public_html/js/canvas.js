var way=1;
window.onload = function () {
    var canvasBig = document.getElementById("canvasBig"),
        canvasWidth = canvasBig.width,
        canvasHeight = canvasBig.height,
        ctxB = canvasBig.getContext("2d"),
        ctxC = document.getElementById("canvasCenter").getContext("2d"),
        ctxS = document.getElementById("canvasSmall").getContext("2d"),
        ctxM = document.getElementById("canvasMan").getContext("2d"),
        ctxT = document.getElementById("canvasText").getContext("2d"),
        list = [ctxB, ctxC, ctxS],
        color = ["red", "orange", "lawngreen"],
        radius = [150, 100, 50, 10],
        startAngle = [(2 * Math.PI), (2.5 * Math.PI), (2 * Math.PI)],
        endAngle = [(Math.PI * 1.5), (Math.PI * 3.7), (Math.PI * 1.5)],
        currentAngle = [0, 90, 180];

    list.forEach(function (item, i) {
        item.lineWidth = 50.0;
        item.strokeStyle = color[i];
    });

    ctxT.fillStyle = 'black';
    ctxT.font = 'italic 10px sans-serif';
    
    function drawTetx() {
        ctxT.beginPath();
        ctxT.clearRect(0, 0, 1000, 1000);
        ctxT.translate(300, 250);
        ctxT.rotate(10 * Math.PI / 180);
        ctxT.translate(-300, -250);
        ctxT.fillText('Button', 285, 250);
        setTimeout(drawTetx, 50);
    }

    function drawMan() {
        var polygon = [
            290, 75, 300, 65, 310, 75, 300, 65, 300, 55,
            290, 45, 300, 55, 310, 45, 300, 55, 300, 45
        ];
        
        ctxM.beginPath();
        ctxM.moveTo(300, 65);
        
        for (var i = 0, l = polygon.length - 1; i < l; i += 2) {
            ctxM.lineTo(polygon[i], polygon[i + 1]);
        }
        
        ctxM.stroke();
        ctxM.beginPath();
        ctxM.arc(300, 40, 5, 0, 2 * Math.PI);
        ctxM.stroke();
    }

    function roll() {
        list.forEach(function (item, i) {
            item.clearRect(0, 0, canvasWidth, canvasHeight);
            item.beginPath();
            item.translate(300, 250);
            item.arc(0, 0, radius[i], startAngle[i] + currentAngle[i], endAngle[i] + currentAngle[i]);
            item.translate(-300, -250);
            item.stroke();
        });

        currentAngle[0] += 0.12 * way;
        currentAngle[1] -= 0.24 * way;
        currentAngle[2] += 0.36 * way;
        setTimeout(roll, 50);
    }
    
    drawMan();
    drawTetx();
    roll();
};

window.onclick = function (event){
    var X = (event.layerX === undefined ? event.offsetX : event.layerX) + 1,
        Y = (event.layerY === undefined ? event.offsetY : event.layerY) + 1,
        result = Math.pow(300 - X, 2) + Math.pow(250 - Y, 2) <= 2500; 
    //Use cached variables like canvasWidth instead of 300, 200 if possible
    	
    if (result) {
        way *= -1;
    }
};