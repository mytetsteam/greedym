(function () {
    var arr = new Array();
    arr[arr.length] = 0;
    var mass =
            [[Math.min(), 25, 14, 78, 18],
                [19, Math.min(), 91, 43, 55],
                [21, 41, Math.min(), 71, 20],
                [31, 5, 13, Math.min(), 4],
                [78, 34, 66, 13, Math.min()]],
            visitedSity = [0],
            result = "(1,",
            numberResult = 0,
            theEnd = false,
            l = mass.length, i;
    method(0);
    function method(num) {
        var min = Math.max.apply(Math, mass[num]),
                vl = visitedSity.length,
                ml = mass.length,
                i, j, indexSity, checked;


        for (i = 0; i < ml; i++) {
            checked = false;
            for (j = 0; j < vl; j++) {
                if (i === visitedSity[j]) {
                    checked = true;
                }
            }

            if (mass[num][i] < min && !checked) {
                indexSity = i;
                min = mass[num][i];
            }
        }

        numberResult += min;
        result += (1 + indexSity) + ")(" + (1 + indexSity) + ",";
        visitedSity.push(indexSity);

        if (visitedSity.length !== 5) {
            method(indexSity);
        } else if (!theEnd) {
            theEnd = true;
            visitedSity.shift();
            method(indexSity);
            result = result.substring(0, result.length - 3);
            function ViewModel() {
                this.result = result;
                this.way = numberResult;
                this.matrix = ko.observableArray([]);
                this.matrix.push({row: mass[0]});
                this.matrix.push({row: mass[1]});
                this.matrix.push({row: mass[2]});
                this.matrix.push({row: mass[3]});
                this.matrix.push({row: mass[4]});
            };
            ko.applyBindings(new ViewModel());
            visitedSity = [0];
            result = "(1,";
            numberResult = 0;
            theEnd = false;
        }
    };
})();
